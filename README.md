# Debate.org

Project to accept debate.org opinion urls and return a summary of the page.

## Project setup

All dependencies can be installed using pip

    pip install -r requirements.txt

## Running project

You can start the project locally by running:

    python app.py

This will create a local server running on port 5000. This server has a single enpoint where you can pass in the url of an opinion page.

    http://localhost:5000/opinions?url=<debate.org url>

Example:

    http://localhost:5000/opinions?url=https://www.debate.org/opinions/is-cyber-bullying-a-real-problem

Sample CURL:

    curl -i -H "Accept: application/json" -X GET \
        http://localhost:5000/opinions?url=https://www.debate.org/opinions/is-cyber-bullying-a-real-problem


## Running tests

Tests are ran using pytest

    python -m pytest tests/
