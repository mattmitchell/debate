import requests

import debate_parser


def fetch_opinion_page(url):
    """Fetches an opinion page and returns the content

    If request is unable to retrieve a valid response None is returned

    Parameters:
        url (str): The url of the opinions page

    Returns:
        text (str): The text of the page requested
    """
    response = requests.get(url, verify=False)

    if response.status_code != 200:
        return None

    return response.text


def scrape_opinion(url):
    """Retrieves the opinion page and returns a summary

    If the page is unable to be retrieved, or the page contains invalid data None
    will be returned from this function.

    Parameters:
        url (str): Url of the opinions page to scrape

    Returns:
        summary (dict): A dict containing the summary of the page
    """
    text = fetch_opinion_page(url)

    if text is None:
        return None

    summary = debate_parser.create_summary(text)

    return summary
