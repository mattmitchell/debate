import re

from flask import Flask, jsonify, request

import opinion_scraper


app = Flask(__name__)


class BaseException(Exception):

    def __init__(self, message):
        Exception.__init__(self)
        self.message = message

    def to_dict(self):
        error_dict = dict(())
        error_dict['message'] = self.message
        return error_dict


class BadRequest(BaseException):
    status_code = 400


class ServerError(BaseException):
    status_code = 500


@app.errorhandler(BaseException)
def handle_bad_request(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@app.route('/opinions', methods=['GET'])
def get_opinions():
    """Gets opinion summary for the url provided

    Parameters:
        url (str): query param that contains the debate.org url to parse
    """
    url = request.args.get('url')

    if url is None:
        raise BadRequest('url query parameter must be provided')

    if not re.match(r'https://(www\.)?debate.org/opinions/', url):
        raise BadRequest('Invalid opinions url provided')

    summary = opinion_scraper.scrape_opinion(url)

    if summary is None:
        raise ServerError('Unable to retrieve or parse opinion page')

    return jsonify(summary)


if __name__ == '__main__':
    app.debug = True
    app.run()
