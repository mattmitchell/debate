import re

from bs4 import BeautifulSoup


def create_opinion(opinion_li):
    """Creates an opinion dict from a BeautifulSoup li Tag

    Parses the contents of an li Tag and returns a dict containing the author,
    headline, argument, like_count, and reply_count.

    If the li Tag is not parseable None will be returned.

    Parameters:
        opinion_li (Tag): BeautifulSoup Tag containing a single opinion li

    Returns:
        opinion (dict): A dict representing an opinion
    """

    # If a li Tag does not have an 'aid' attribute it is not an opinion
    if not opinion_li.has_attr('aid'):
        return None

    argument = opinion_li.p.get_text()
    headline = opinion_li.h2.get_text()
    likes = int(opinion_li.find('div', {'class': 'l-cnt'}).get_text())
    messages = int(opinion_li.find('div', {'class': 'm-cnt'}).get_text())

    # Author does not always exist in valid opinions
    author = opinion_li.cite.a.get_text() if opinion_li.cite.find('a') else None

    return {'argument': argument, 'author': author, 'headline': headline, 'likes': likes, 'messages': messages}


def create_opinion_list(opinions_ul):
    """Creates a list of opinions from a BeautifulSoup ul Tag

    All debate opinions are contained within ul Tag.

    Parameters:
        opinions_ul (Tag): BeautifulSoup Tag containing a ul Tag

    Returns:
        opinions (list): A list of opinion dicts
    """

    opinions = []

    for opinion_li in opinions_ul.find_all('li'):
        opinion = create_opinion(opinion_li)

        if opinion is not None:
            opinions.append(opinion)

    return opinions


def get_opinion_title(soup_html):
    """Finds the title of the opinion page

    Parameters:
        soup_html: BeautifulSoup html doc for the opinion page

    Returns:
        title (str): The title of the page
    """
    title_meta_tag = soup_html.find(property='og:title')
    return title_meta_tag['content']


def get_opinion_percent(soup_html):
    """Finds the percentage of yes/no opinions

    Parameters:
        soup_html (Tag): BeautifulSoup html doc for the opinion page

    Returns:
        percent (dict): Percent dict containing 'yes' and 'no' int attributes
    """

    def extract_percent(tag):
        return int(re.search(r'^\d+', tag.text).group())

    yes_span = soup_html.find('span', {'class': 'yes-text'})
    yes_percent = extract_percent(yes_span)

    no_span = soup_html.find('span', {'class': 'no-text'})
    no_percent = extract_percent(no_span)

    return {'yes': yes_percent, 'no': no_percent}


def create_summary(html):
    """Creates a summary document from the html string provided

    Parameters:
        html (str): String containing the html of an opinions page

    Returns:
        summary (dict): Dictionary containing a summary of the page
    """
    soup = BeautifulSoup(html, 'html.parser')

    yes_opinions = create_opinion_list(soup.find(id='yes-arguments').ul)
    no_opinions = create_opinion_list(soup.find(id='no-arguments').ul)
    title = get_opinion_title(soup)
    percent = get_opinion_percent(soup)

    return {
        'yes_opinions': yes_opinions,
        'no_opinions': no_opinions,
        'title': title,
        'percent': percent
        }
