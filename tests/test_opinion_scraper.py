from unittest import mock

import pytest
import requests

import debate_parser
import opinion_scraper


@pytest.fixture(autouse=True)
def mock_requests(monkeypatch):
    """Fixture for mocking requests

    The response is determined by the url being requested. To create a new mock response
    update the mock_responses object with a new key matching the url and a value containing
    a dict of the properties of the mock requests.Response object.
    """

    mock_responses = {
        'http://debate.org/opinions/valid-sample': {
            'status_code': 200,
            'text': '<html></html>'
        },
        'http://debate.org/opinions/invalid-sample': {
            'status_code': 404,
            'text': 'Not found'
        }
    }

    def mock_get(url, verify=False):
        m = mock.Mock()
        m.configure_mock(**mock_responses[url])
        return m

    monkeypatch.setattr(requests, 'get', mock_get)


@pytest.fixture(autouse=True)
def mock_debate_parser(monkeypatch):
    monkeypatch.setattr(debate_parser, 'create_summary', lambda url: {'mock': 'summary'})


def test_fetch_opinion_page_returns_text():
    text = opinion_scraper.fetch_opinion_page('http://debate.org/opinions/valid-sample')
    assert text == '<html></html>'


def test_fetch_opinion_page_returns_none_if_response_not_200():
    text = opinion_scraper.fetch_opinion_page('http://debate.org/opinions/invalid-sample')
    assert text is None


def test_scrape_opinion_returns_content_from_parser():
    summary = opinion_scraper.scrape_opinion('http://debate.org/opinions/valid-sample')
    assert summary == {'mock': 'summary'}


def test_scrape_opinion_returns_none_if_page_not_fetched():
    summary = opinion_scraper.scrape_opinion('http://debate.org/opinions/invalid-sample')
    assert summary is None
