import json

import pytest

import app
import opinion_scraper


@pytest.fixture
def client():
    client = app.app.test_client()

    yield client


@pytest.fixture(autouse=True)
def mock_scraper(monkeypatch):

    def mock_summary(url):
        if url == 'https://debate.org/opinions/servererror':
            return None
        return {'mock': 'response'}

    monkeypatch.setattr(opinion_scraper, 'scrape_opinion', mock_summary)


def test_opinions_returns_json_object(client):
    response = client.get('/opinions?url=https://debate.org/opinions/test')
    assert json.loads(response.data) == {'mock': 'response'}


def test_opinions_returns_json_for_www_url(client):
    response = client.get('/opinions?url=https://www.debate.org/opinions/test')
    assert json.loads(response.data) == {'mock': 'response'}


def test_opinions_returns_400_if_missing_url(client):
    response = client.get('/opinions')
    assert response.status_code == 400
    assert response.get_json()['message'] == 'url query parameter must be provided'


def test_opinions_returns_400_if_invalid_url(client):
    response = client.get('/opinions?url=https://debate.org/debates/test')
    assert response.status_code == 400
    assert response.get_json()['message'] == 'Invalid opinions url provided'


def test_opinions_returns_500_if_summary_is_none(client):
    response = client.get('/opinions?url=https://debate.org/opinions/servererror')
    assert response.status_code == 500
    assert response.get_json()['message'] == 'Unable to retrieve or parse opinion page'
