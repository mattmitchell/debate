from os import path
from unittest import mock

import pytest
import requests

import app


fixtures_dir = path.abspath(path.join(path.dirname(__file__), 'fixtures'))


@pytest.fixture(autouse=True)
def mock_requests(monkeypatch):

    def mock_responses(url, verify=False):
        file_name = url.lstrip('https://debate.org/opinions') + '.html'
        html_doc = path.join(fixtures_dir, file_name)
        m = mock.Mock()

        with open(html_doc) as html:
            m.configure_mock(status_code=200, text=html.read())
        return m

    monkeypatch.setattr(requests, 'get', mock_responses)


@pytest.fixture
def client():
    client = app.app.test_client()

    yield client


def test_get_opinions_returns_expected_json(client):
    response = client.get('/opinions?url=https://debate.org/opinions/full_page')
    expected = {
        'yes_opinions': [
            {
                'argument': 'They help catch bugs',
                'headline': 'Tests are the best',
                'author': 'Test_Supporter',
                'likes': 3,
                'messages': 2
            },
            {
                'argument': 'Otherwise things break',
                'headline': 'Tests are necessary',
                'author': 'SuperTest',
                'likes': 4,
                'messages': 1
            }
        ],
        'no_opinions': [
            {
                'argument': 'I could be adding functionality instead.',
                'headline': 'Tests waste time',
                'author': None,
                'likes': 0,
                'messages': 100
            }
        ],
        'title': 'Should Tests Be Written?',
        'percent': {
            'yes': 75,
            'no': 25
        }
    }

    assert response.get_json() == expected
