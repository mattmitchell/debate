from os import path

import pytest
from bs4 import BeautifulSoup

import debate_parser


fixtures_dir = path.abspath(path.join(path.dirname(__file__), 'fixtures'))


@pytest.fixture
def opinion_li():
    """Returns a BeautifulSoup li Tag created from fixture html document
    """
    opinion_html_file = path.join(fixtures_dir, 'valid_opinion_li.html')

    with open(opinion_html_file) as opinion_html:
        return BeautifulSoup(opinion_html, 'html.parser').li


@pytest.fixture
def opinions_ul():
    """Returns a BeautifulSoup ul Tag containing a list of opinions
    """
    opinions_html_file = path.join(fixtures_dir, 'valid_opinions_ul.html')

    with open(opinions_html_file) as opinions_html:
        return BeautifulSoup(opinions_html, 'html.parser').ul


@pytest.fixture
def full_page_html():
    """Returns a string containing the full html contents of a page
    """
    full_page_file = path.join(fixtures_dir, 'full_page.html')

    with open(full_page_file) as full_html:
        return full_html.read()


@pytest.fixture
def soup_html(full_page_html):
    """Returns a BeautifulSoup html Tag for full html page
    """
    return BeautifulSoup(full_page_html, 'html.parser')


def test_opinion_without_aid_attribute_returns_none():
    opinion_li = BeautifulSoup('<li></li>', 'html.parser').li
    opinion = debate_parser.create_opinion(opinion_li)
    assert opinion is None


def test_opinion_extracts_argument(opinion_li):
    opinion = debate_parser.create_opinion(opinion_li)
    assert opinion['argument'] == 'They are useful'


def test_opinion_extracts_author(opinion_li):
    opinion = debate_parser.create_opinion(opinion_li)
    assert opinion['author'] == 'SomeUser'


def test_opinion_without_author(opinion_li):
    opinion_li.cite.clear()
    opinion = debate_parser.create_opinion(opinion_li)
    assert opinion['author'] is None


def test_opinion_extracts_headline(opinion_li):
    opinion = debate_parser.create_opinion(opinion_li)
    assert opinion['headline'] == 'Yes, tests should be written'


def test_opinion_extracts_likes(opinion_li):
    opinion = debate_parser.create_opinion(opinion_li)
    assert opinion['likes'] == 1


def test_opinion_extracts_messages(opinion_li):
    opinion = debate_parser.create_opinion(opinion_li)
    assert opinion['messages'] == 3


def test_opinions_returns_list_of_opinions(opinions_ul):
    opinions = debate_parser.create_opinion_list(opinions_ul)
    expected = [
        {
            'headline': 'First headline',
            'argument': 'First argument',
            'author': 'First_Author',
            'likes': 5,
            'messages': 5
        }, {
            'headline': 'Second headline',
            'argument': 'Second argument',
            'author': 'Second_Author',
            'likes': 6,
            'messages': 2
        },
    ]
    assert opinions == expected


def test_opinions_does_not_add_none():
    opinions_ul = BeautifulSoup('<ul><li></li><li></li></ul>', 'html.parser').ul
    opinions = debate_parser.create_opinion_list(opinions_ul)
    assert opinions == []


def test_get_title_returns_meta_tag_title(soup_html):
    title = debate_parser.get_opinion_title(soup_html)
    assert title == 'Should Tests Be Written?'


def test_get_opinion_percent_returns_yes_percentage(soup_html):
    percentages = debate_parser.get_opinion_percent(soup_html)
    assert percentages['yes'] == 75


def test_get_opinion_percent_returns_no_percentage(soup_html):
    percentages = debate_parser.get_opinion_percent(soup_html)
    assert percentages['no'] == 25


def test_summary_should_contain_yes_opinions(full_page_html):
    summary = debate_parser.create_summary(full_page_html)
    assert len(summary['yes_opinions']) == 2


def test_summary_should_contain_no_opinions(full_page_html):
    summary = debate_parser.create_summary(full_page_html)
    assert len(summary['no_opinions']) == 1


def test_summary_should_contain_title(full_page_html):
    summary = debate_parser.create_summary(full_page_html)
    assert summary['title'] == 'Should Tests Be Written?'


def test_summary_should_contain_percent(full_page_html):
    summary = debate_parser.create_summary(full_page_html)
    assert summary['percent'] == {'yes': 75, 'no': 25}
